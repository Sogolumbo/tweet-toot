#!/usr/bin/env python3

import sys
import logging
from collections import OrderedDict
from hashlib import sha1
import datetime
import time
import requests
from bs4 import BeautifulSoup

import mimetypes

if sys.version_info.major == 3:
    from urllib.parse import urlencode, urlparse, urlunparse, parse_qs
else:
    from urllib import urlencode
    from urlparse import urlparse, urlunparse, parse_qs


import helpers

logger = logging.getLogger(__name__)
non_bmp_map = dict.fromkeys(range(0x10000, sys.maxunicode + 1), 0xfffd)


class TweetToot:

    app_name = ""
    twitter_url = ""
    mastodon_url = ""
    mastodon_token = ""


    def __init__(
        self, app_name: str, twitter_url: str, mastodon_url: str, mastodon_token: str, fallback_to_link_on_failed_media_upload: bool = True, use_nitter_urls: bool = True
    ):

        self.app_name = app_name
        self.twitter_url = twitter_url
        self.mastodon_url = mastodon_url
        self.mastodon_token = mastodon_token
        self.fallback_to_link_on_failed_media_upload = fallback_to_link_on_failed_media_upload
        self.use_nitter_urls = use_nitter_urls

    def relay(self):

        """ Main code which relays tweets to the Mastodon instance.

        :type self:
        :param self:
    
        :raises:
    
        :rtype: bool
        """

        if not self.app_name:
            logger.error(f"relay() => Application name in config is incorrect/empty.")

            return False

        if not self.twitter_url:

            logger.error(f"relay() => Twitter URL in config is incorrect/empty.")

            return False

        if not self.mastodon_url:

            logger.error(f"relay() => Mastodon URL in config is incorrect/empty.")

            return False

        if not self.mastodon_token:

            logger.error(f"relay() => Mastodon token in config is incorrect/empty.")

            return False

        logger.info(
            f"relay() => Init relay from {self.twitter_url} to {self.mastodon_url}. State file {self._get_timestamp_file_path()}"
        )

        tweets = self._get_tweets()

        if not tweets:

            return True

        logger.debug(f"relay() => {str(tweets).translate(non_bmp_map)}")

        last_timestamp = 0

        for tweet_id, tweet in tweets.items():
            if tweet_id < last_timestamp:
                break;

            logger.info(f"relay() => Tooting {tweet['id']} to {self.mastodon_url}")

            last_timestamp = (
                tweet_id if tweet_id > last_timestamp else last_timestamp
            )

            media_ids = None
            if tweet["media"]:
                media_ids = list()
                for media_url in tweet["media"]:
                    if "status" in media_url:
                        logger.info(f"Media url was ignored because the according media can not be downloaded: {media_url}")
                        continue
                    media_id = self._upload_media(
                        mastodon_url=self.mastodon_url,
                        media_url=media_url
                    )
                    if media_id: #successful upload of the media
                         media_ids.append(media_id)
                    else:
                        message = "Could not upload media for toot " + str(tweet_id) + " (" + media_url + ")"
                        if self.fallback_to_link_on_failed_media_upload:
                            logging.warning(message +" Fallback enabled (adding media link to the text)")
                            tweet["text"] += ("\n\U0001F5BC: " + media_url) #picture emoji
                        else:
                            logging.error(message +"\nHence the relay action is stopped.")
                            return False
                
            success = self._toot_the_tweet(
                mastodon_url=self.mastodon_url,
                tweet_id=str(tweet["id"]),
                tweet_body=tweet["text"],
                media_ids=media_ids
            )
            if not success:
                return False

            self._set_last_timestamp(timestamp=last_timestamp)

    def debug(self):
        
        """ debugs tweet scraping.

        :type self:
        :param self:
    
        :raises:
    
        :rtype: bool
        """

        if not self.app_name:

            logger.error(f"debug() => Application name in config is incorrect/empty.")

            return False

        if not self.twitter_url:

            logger.error(f"debug() => Twitter URL in config is incorrect/empty.")

            return False

        if not self.mastodon_url:

            logger.error(f"debug() => Mastodon URL in config is incorrect/empty.")

            return False

        if not self.mastodon_token:

            logger.error(f"debug() => Mastodon token in config is incorrect/empty.")

            return False

        logger.info(
            f"debug() => Init relay from {self.twitter_url} to {self.mastodon_url}. State file {self._get_timestamp_file_path()}"
        )

        tweets = self._get_tweets()

        if not tweets:

            return True

        logger.debug(f"debug() => {str(tweets).translate(non_bmp_map)}")

        last_timestamp = 0

        for tweet_id, tweet in tweets.items():
            if tweet_id < last_timestamp:
                break;

            last_timestamp = (
                tweet_id if tweet_id > last_timestamp else last_timestamp
            )

            logger.info('Would toot id: ' + str(tweet["id"]) + ' time: ' + str(tweet_id) +'\n'+ tweet["text"].translate(non_bmp_map) + 'media:' + str(tweet["media"]));
            
            self._set_last_timestamp(timestamp=last_timestamp)

    def _get_tweets(self):
        """ Get list of new tweets, with tweet ID and content, from configured Twitter account URL.
        This function relies on BeautifulSoup to extract the tweet IDs and content of all tweets on the specified page.
        The data is returned as a list of dictionaries that can be used by other functions.

        :type self:
        :param self:

        :raises:

        :rtype: dict
        """

        tweets = OrderedDict()
        last_timestamp = self._get_last_timestamp()

        headers = {}
        headers["accept-language"] = "en-US,en;q=0.9"
        headers["dnt"] = "1"
        headers["user-agent"] = self.app_name

        data = requests.get(self.twitter_url)
        ### save html page for development and debugging
        #with open("debugging.html",'w', encoding='utf-8') as file:
        #    file.write("html-page:\n" + data.text)
        html = BeautifulSoup(data.text, "html.parser")
        timeline = html.select("table.tweet")

        if timeline is None:

            logger.error(
                f"get_tweets() => Could not retrieve tweets from the page. Please make sure the source Twitter URL ({self.twitter_url}) is correct."
            )
            return False

        logger.info(
            f"get_tweets() => Fetched {len(timeline)} tweets for {self.twitter_url}."
        )

        for tweet in timeline:

            try:
                tweet_text_html = tweet.find('div', class_="tweet-text");
                tweet_id = int(tweet_text_html.attrs["data-id"]);

                if tweet_id > last_timestamp:

                    tweet_text = tweet_text_html.select("div > div")[0].get_text()

                    media_urls = None

                    tmp = tweet_text_html.select("div > div .twitter_external_link")

                    # fix urls in links
                    a_tags = tweet_text_html.select("a.twitter_external_link")
                    if len(a_tags) > 0:
                        for at in a_tags:
                            url = f'{at["data-url"]} '
                            
                            u = urlparse(url)
                            query = parse_qs(u.query, keep_blank_values=True)
                            #remove tracking parameters
                            query.pop('Echobox', None)
                            query.pop('utm_source', None)
                            u = u._replace(query=urlencode(query, True))
                            if 'Echobox=' in u.fragment:
                                u = u._replace(fragment='')
                            #replace twitter.com with nitter.net
                            if self.use_nitter_urls and 'twitter.com' in u.netloc:
                                u = u._replace(netloc='nitter.net')
                            url = urlunparse(u)

                            
                            if (str(tweet_id) in url) and ("photo" in url or "video" in url):
                                #handle photos and videos
                                if not media_urls:
                                    media_urls = self._get_media_urls(url);
                                if not url in media_urls: #if the url is part of media_urls it's not possible to download the media
                                    tweet_text = str(tweet_text).replace(at.get_text(), '')
                                    logger.debug('link removed (media link): ' + at.get_text())
                                else:
                                    tweet_text = str(tweet_text).replace(at.get_text(), "\n\U0001F5BC: " + url) #picture emoji
                                    logger.debug('media link repaired: ' + at.get_text() + ' -> ' + url);
                            else:
                                tweet_text = str(tweet_text).replace(at.get_text(), url)
                                logger.debug('link repaired: ' + at.get_text() + ' -> ' + url);

                    #mark retweet & answer
                    tweet_is_retweet = (tweet.find('td', class_="tweet-social-context") is not None)
                    tweetHeader = ''
                    tweetFooter = ''
                    if tweet_is_retweet:
                        authorElement = tweet.find('td', class_='user-info').a;
                        author = authorElement.find('strong', class_='fullname').get_text() + ' (' + authorElement.find('div', class_='username').text.replace('\n','').replace(' ', '') + ')'
                        tweetHeader += ('RT ' + author +'\n')
                    tweet_is_answer = (tweet.find('div', class_="tweet-reply-context username") is not None)
                    if tweet_is_answer:
                        original_authors_elements = tweet.find('div', class_="tweet-reply-context username").find_all('a');
                        for item in original_authors_elements:
                            if item.find('a') is not None:
                                original_authors_elements.remove(item)
                        original_authors = [" ".join(item.text.split()) for item in original_authors_elements]
                        original_authors_text = ", ".join(original_authors)
                        tweetHeader += ('Replying to: ' + original_authors_text +'\n')
                    if tweet_is_retweet or tweet_is_answer:
                        tweet_link = ("https://twitter.com" if not self.use_nitter_urls else "https://nitter.net") + str(tweet.attrs["href"]).split('?')[0]
                        tweetFooter += u'\n\U0001F426\U0001F517: ' + tweet_link + '\n' #bird and link emojis
                        tweet_text = tweetHeader + tweet_text + tweetFooter
                        
                    #shorten if necessary
                    if len(tweet_text)>500:
                        tweet_text = tweet_text[:497]+'...'

                    tweets[tweet_id] = {"id": tweet_id, "text": tweet_text, "media": media_urls};
                else:
                    logger.debug("Too old: id " + str(tweet_id))

            except Exception as e:

                logger.error("get_tweets() => An error occurred.")
                logger.error(e)

                continue

        return (
            {k: tweets[k] for k in sorted(tweets, reverse=(last_timestamp== 0))}
            if len(tweets) > 0
            else None
        )

    def _get_media_urls(self, original_tweet_url):
        headers = {}
        headers["accept-language"] = "en-US,en;q=0.9"
        headers["dnt"] = "1"
        headers["user-agent"] = self.app_name

        tweet_url = original_tweet_url.replace('https://twitter', 'https://mobile.twitter')[:-8] #remove /photo/1/ at the end of the url

        data = requests.get(tweet_url)
        ### save html page for development and debugging
        #with open("debugging_media.html",'w', encoding='utf-8') as file:
        #    file.write("html-page: <a href=\"" + tweet_url + "\">source</a>\n" + data.text)
        html = BeautifulSoup(data.text, "html.parser")
        allMediaItems = html.select("div.media")

        urls = list()
        for media in allMediaItems:
            img = media.find('img')
            url = img['src']
            if "tweet_video_thumb" in url:
                url = url.replace('pbs.twimg.com/tweet_video_thumb/', 'video.twimg.com/tweet_video/').replace('.jpg','.mp4')
            elif "ext_tw_video_thumb" in url:
                urls.append(original_tweet_url) #Adding the original url means that it's not possible to download the media
                continue
            url = url.replace(':small','')
            urls.append(url)
        logger.debug(f'media from {tweet_url}: {", ".join(urls)}')
        return urls
        

    def _get_last_timestamp(self):

        """ Get the last tweet's timestamp.

        :type self:
        :param self:

        :raises:

        :rtype: int
        """

        ts = helpers._read_file(self._get_timestamp_file_path())

        return int(ts) if ts else 0

    def _set_last_timestamp(self, timestamp: int):

        """ Set the last tweet's timestamp.

        :type self:
        :param self:

        :type timestamp:int:
        :param timestamp:int: Timestamp of current tweet.

        :raises:

        :rtype: bool
        """

        return helpers._write_file(self._get_timestamp_file_path(), str(timestamp))

    def _get_timestamp_file_path(self):

        """ Get file path that stores tweet timestamp.

        :type self:
        :param self:

        :raises:

        :rtype: str
        """

        return (
            helpers._config("TT_CACHE_PATH")
            + "tt_"
            + sha1(
                self.twitter_url.encode("utf-8") + self.mastodon_url.encode("utf-8")
            ).hexdigest()
        )

    def _toot_the_tweet(
        self, mastodon_url: str, tweet_id: str, tweet_body: str, media_ids = None
    ):

        """ Receieve a dictionary containing Tweet ID and text... and TOOT!
        This function relies on the requests library to post the content to your Mastodon account (human or bot).
        A boolean success status is returned.
            
        :type self:
        :param self:
    
        :type tweet_id:str:
        :param tweet_id:str: Tweet ID.
    
        :type tweet_body:str:
        :param tweet_body:str: Tweet text.

        :raises:
    
        :rtype: bool
        """
        
        headers = {}
        headers["Authorization"] = f"Bearer {self.mastodon_token}"
        headers["Idempotency-Key"] = tweet_id

        data = {}
        data["status"] = tweet_body
        data["visibility"] = "public"
        if media_ids and len(media_ids)>0:
            data["media_ids[]"] = media_ids
            logger.debug("Tooting with media: " + str(media_ids))
        else:
            logger.debug("Tooting without media: " + str(media_ids))

        response = requests.post(
            url=f"{mastodon_url}/api/v1/statuses", data=data, headers=headers
        )

        if response.status_code == 200:

            logger.info(
                f"toot_the_tweet() => OK."
            )
            logger.debug(f"toot_the_tweet() => Response: {response.text}")

            return True

        else:

            logger.error(
                f"toot_the_tweet() => Could not toot {tweet_id} to {self.mastodon_url}."
            )
            logger.error(f"toot_the_tweet() => Response: {response.text}")

            return False
    def _upload_media(
        self, mastodon_url: str, media_url: str
    ):
        filetype = media_url.split('.')[-1]
        filename = 'media.' + filetype
        mimetype = mimetypes.guess_type(media_url)

        #download file
        mediaResponse = requests.get(media_url)

        with open(filename, 'wb') as file:
            for chunk in mediaResponse.iter_content(chunk_size=128):
                file.write(chunk)

        #upload file
        headers = {}
        headers["Authorization"] = f"Bearer {self.mastodon_token}"
        
        data = {}

        files = dict(file=(filename, open(filename, mode='rb'), mimetype))
        
        response = requests.post(
            #url=f"{mastodon_url}", data=data, headers=headers, files=files   #debugging
            url=f"{mastodon_url}/api/v1/media", data=data, headers=headers, files=files
        )
        jsonResponse = response.json()

        if response.status_code == 200:

            logger.info(
                f"_upload_media() => uploaded as {str(jsonResponse['id'])}"
            )
            logger.debug(f"_upload_media() => Response: {response.text}")

            return jsonResponse["id"]

        else:

            logger.warning(
                f"_upload_media() => Could not upload media {media_url} to {self.mastodon_url}."
            )
            logger.debug(f"_upload_media() => Response: {response.text}")

            return False
